# Skooldio First Round Test

Run a project in the development server with:

```sh
npm start
```

or build with:

```sh
npm run build
```

and serve `build/` folder using any server you like.
