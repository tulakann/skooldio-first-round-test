import React from 'react';
import { createGlobalStyle } from 'styled-components';
import { Helmet } from 'react-helmet';
import InfoCard from './components/InfoCard';
import data from './data/index';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: 'Prompt', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .App {
    padding: 16px;
  }
`;

function App() {
  return (
    <>
      <Helmet>
        <title>Skooldio First Round Test</title>
        <link
          href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,300;0,400;0,500;0,600;1,300&display=swap"
          rel="stylesheet"
        />
      </Helmet>
      <GlobalStyle />
      <div className="App">
        <InfoCard data={data} />
      </div>
    </>
  );
}

export default App;
