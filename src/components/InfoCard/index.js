import PropTypes from 'prop-types';
import React from 'react';
import { thousandSeparator } from '../../utils';
import PlayIcon from '../Icons/PlayIcon';
import SvgLoader from '../Icons/SvgLoader';
import UserIcon from '../Icons/UserIcon';
import RoundNumber from './RoundNumber';
import StyledContainer from './StyledContainer';

const InfoCard = ({ data }) => {
  const {
    logo,
    faculty,
    department,
    university,
    availableRounds,
    roundInfo: {
      no: roundNo,
      title: roundTitle,
    },
    score,
    min60,
    mean60,
    max60,
    interestedCount,
  } = data;

  if (!data) {
    return null;
  }

  return (
    <StyledContainer>
      <section className="header">
        <SvgLoader icon={logo} />
        <div className="infoBlock">
          <h3>{faculty}</h3>
          <h4>{department}</h4>
          <span>{university}</span>
        </div>
        <SvgLoader className="love-icon" icon="love" />
      </section>
      <hr />
      <section className="main">
        <div className="row first">
          <span className="available-rounds-label">รอบที่เปิด</span>
          {
            Array.isArray(availableRounds) && availableRounds.length > 0
              ? (
                <div className="available-rounds-container">
                  {[1, 2, 3, 4, 5].map((num) => (
                    <RoundNumber
                      key={num}
                      number={num}
                      active={availableRounds.includes(num)}
                    />
                  ))}
                </div>
              )
              : null
          }
        </div>
        <div className="row second">
          <span>
            {`รอบที่ ${roundNo} : ${roundTitle}`}
          </span>
          <button type="button">
            <span>แก้ไขคะแนน</span>
            <SvgLoader
              icon="mathOps"
              imgStyle={`
                display: block;
                margin: auto 0 1px 5px;
              `}
            />
          </button>
        </div>
        <div className="row third">
          <div className="left">
            <SvgLoader
              icon="ribbon"
              imgStyle={`
                display: block;
                margin: auto 0 1px 5px;
              `}
            />
          </div>
          <div className="right">
            <span className="label">คะแนนของคุณคือ</span>
            <span className="score">{thousandSeparator(score)}</span>
          </div>
        </div>
        <div className="row fourth">
          <div className="column">
            <span>{thousandSeparator(min60)}</span>
            <span>คะแนนต่ำสุด 60</span>
          </div>
          <div className="vr" />
          <div className="column">
            <span>{thousandSeparator(mean60)}</span>
            <span>คะแนนเฉลี่ย 60</span>
          </div>
          <div className="vr" />
          <div className="column">
            <span>{thousandSeparator(max60)}</span>
            <span>คะแนนสูงสุด 60</span>
          </div>
        </div>
      </section>
      <hr />
      <section className="details">
        <PlayIcon color="#48b6a3" />
        <span>ดูสัดส่วนคะแนน</span>
      </section>
      <hr />
      <section className="share">
        <div className="left">
          <UserIcon color="#9b9b9b" />
          <span>{interestedCount}</span>
          <span>คนที่สนใจ</span>
        </div>
        <div className="right">
          <button type="button">
            <SvgLoader icon="share" />
          </button>
        </div>
      </section>
    </StyledContainer>
  );
};

InfoCard.propTypes = {
  data: PropTypes.shape({
    logo: PropTypes.string,
    faculty: PropTypes.string,
    department: PropTypes.string,
    university: PropTypes.string,
    availableRounds: PropTypes.arrayOf(PropTypes.number),
    roundInfo: PropTypes.shape({
      no: PropTypes.number,
      title: PropTypes.string,
    }),
    score: PropTypes.number,
    min60: PropTypes.number,
    mean60: PropTypes.number,
    max60: PropTypes.number,
    interestedCount: PropTypes.number,
  }),
};

InfoCard.defaultProps = {
  data: undefined,
};

export default InfoCard;
