import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  width: 29px;
  height: 29px;
  border-radius: 50%;
  background-color: ${(props) => (props.active ? '#2ecc71' : '#d8d8d8')};
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    color: white;
    font-size: 18.4px;
    font-weight: 500;
    text-align: center;
  }
`;

const RoundNumber = ({ active, number }) => (
  number
    ? (
      <Container active={active}>
        <span>{number}</span>
      </Container>
    )
    : null);

RoundNumber.propTypes = {
  active: PropTypes.bool,
  number: PropTypes.number,
};

RoundNumber.defaultProps = {
  active: false,
  number: undefined,
};

export default RoundNumber;
