import styled from 'styled-components';

/**
 * Most of the styles of InfoCard are here, it is separated into sections and blocks.
 * Most of the positional layout is done via flexbox.
 */
const StyledContainer = styled.div`
  width: 442px;
  max-width: 442px;
  /* height: 493px; */
  border-radius: 5px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.3);
  background-color: rgba(255, 255, 255, 0);
  padding: 16px;

  section.header {
    display: flex;
    flex-direction: row;
    position: relative;

    div.infoBlock {
      margin-left: 16px;

      /* reset default margin */
      h3, h4 {
        margin: 0;
      }

      h3 {
        font-size: 24px;
        font-weight: 600;
        line-height: 0.67;
        color: #ff5a5a;

        margin-bottom: 8px;

        /* this is hardcoded, better go with specific design for breakpoints */
        @media (max-width: 420px) {
          line-height: 1;
        }
      }

      h4 {
        font-size: 20px;
        font-weight: 500;
        line-height: 0.8;
        color: #9b9b9b;

        margin-bottom: 24px;
      }

      span {
        font-size: 20px;
        font-weight: 300;
        line-height: 0.8;
        color: #9b9b9b;
      }
    }

    .love-icon {
      position: absolute;
      top: 0;
      right: 0;
    }
  }

  section.main {
    margin-top: 16px;

    .row {
      display: flex;
      flex-direction: row;
      align-items: center;
    }

    .row.first {
      margin-bottom: 12px;

      .available-rounds-label {
        font-size: 20px;
        font-weight: 300;
        line-height: 1.01;
        color: #5f5f5f;

        margin-right: 24px;
      }

      .available-rounds-container {
        display: flex;
        flex-direction: row;

        div {
          margin-right: 8px;
        }
      }
    }

    .row.second {
      justify-content: space-between;

      margin-bottom: 24px;

      span {
        font-size: 16px;
        font-weight: 600;
        line-height: 1;
        color: #ff5a5a;
      }

      button {
        width: 120px;
        height: 35px;
        border-radius: 17.5px;
        background-color: transparent;
        border: 1px solid #ff5a5a;
        outline: none;

        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;

        span {
          font-family: Prompt;
          font-size: 12px;
          font-weight: 500;
          text-align: center;
          color: #ff5a5a;
          outline: none;

          margin-right: 8px;
        }

        :hover {
          span {
            color: #666;
          }

          border: 1px solid #666;
        }

        :active {
          span {
            color: #ff5a5a;
          }

          border: 1px solid #ff5a5a;
        }
      }
    }

    .row.third {
      justify-content: space-between;

      margin-bottom: 18px;

      .left {
        margin: 0 32px;
      }

      .right {
        display: flex;
        flex-direction: column;
        align-items: flex-end;

        font-weight: 300;
        color: #4a4a4a;

        span.label {
          font-size: 12px;
        }

        span.score {
          font-size: 42px;
          line-height: 1.18;
        }
      }
    }

    .row.fourth {
      justify-content: space-around;

      margin-bottom: 16px;

      .column {
        display: flex;
        flex-direction: column;
        align-items: center;

        span {
          font-size: 11.2px;
          font-weight: normal;
          line-height: 1.5;
          color: #5f5f5f;

          /* enlarged number */
          :nth-child(1) {
            font-size: 19.6px;
          }
        }
      }

      /* vertical separated line */
      .vr {
        height: 32px;
        border-right: 1px solid #d8d8d8;
      }
    }
  }

  section.details {
    display: flex;
    align-items: center;

    margin: 18px 0;

    span {
      font-size: 16px;
      font-weight: 300;
      line-height: 1;
      color: #48b6a3;

      margin-left: 18px;
    }
  }

  section.share {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    margin-top: 12px;

    .left {
      display: flex;
      align-items: center;

      span {
        font-size: 12px;
        font-weight: 300;
        line-height: 1.25;
        color: #9b9b9b;

        margin-left: 8px;

        :last-child {
          font-style: italic;
        }
      }
    }

    .right {
      button {
        background-color: transparent;
        border: none;
        outline: none;
        cursor: pointer;
      }
    }
  }

  hr {
    border-top: 1px solid #d8d8d8;
  }
`;

export default StyledContainer;
