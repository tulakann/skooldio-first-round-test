// taken from https://iconsvg.xyz/
import React from 'react';
import PropTypes from 'prop-types';

const PlayIcon = ({ size, color }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    fill="none"
    stroke={color}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <polygon points="5 3 19 12 5 21 5 3" />
  </svg>
);

PlayIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

PlayIcon.defaultProps = {
  size: 16,
  color: '#000',
};

export default PlayIcon;
