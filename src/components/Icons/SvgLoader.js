import React from 'react';
import PropTypes from 'prop-types';
// this is used to make css prop working
// eslint-disable-next-line no-unused-vars
import styled from 'styled-components/macro';

const resourcesMap = {
  engineerGear: {
    src: require('../../assets/svgs/engineer-gear.svg'),
    alt: 'gear logo',
  },
  mathOps: {
    src: require('../../assets/svgs/math-ops.svg'),
    alt: 'math operators',
  },
  ribbon: {
    src: require('../../assets/svgs/ribbon.svg'),
    alt: 'ribbon icon',
  },
  share: {
    src: require('../../assets/svgs/share.svg'),
    alt: 'share botton',
  },
  love: {
    src: require('../../assets/svgs/love.svg'),
    alt: 'love icon',
  },
};

/**
 * SvgLoader - this jsdocs allows vscode intellisense
 * to suggest from enums
 * @param {object} props component props
 * @param {('engineerGear'|'mathOps'|'ribbon'|'share'|'love')} props.icon icon name to be loaded,
 * if not available will render null
 * @param {string} props.imgStyle string of style to pass to css prop of the image
 */
const SvgLoader = ({
  icon, imgStyle, ...restProps
}) => {
  const availableIcons = Object.keys(resourcesMap);

  if (!availableIcons.includes(icon)) {
    return null;
  }

  const { src, alt } = resourcesMap[icon];

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <div {...restProps}>
      <img
        css={imgStyle}
        src={src}
        alt={alt}
      />
    </div>
  );
};

SvgLoader.propTypes = {
  icon: PropTypes.oneOf([
    'engineerGear',
    'mathOps',
    'ribbon',
    'share',
    'love',
  ]).isRequired,
  imgStyle: PropTypes.string,
};

SvgLoader.defaultProps = {
  imgStyle: undefined,
};

export default SvgLoader;
