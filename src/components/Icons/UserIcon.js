// taken from https://iconsvg.xyz/
import React from 'react';
import PropTypes from 'prop-types';

const UserIcon = ({ size, color }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    fill="none"
    stroke={color}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
    <circle cx="12" cy="7" r="4" />
  </svg>
);

UserIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

UserIcon.defaultProps = {
  size: 16,
  color: '#000',
};

export default UserIcon;
