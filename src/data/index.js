const dummyInfo = {
  logo: 'engineerGear',
  faculty: 'คณะวิศวกรรมศาสตร์',
  department: 'สาขาวิศวกรรมทั่วไป',
  university: 'จุฬาลงกรณ์มหาวิทยาลัย',
  availableRounds: [1, 2, 4],
  roundInfo: {
    no: 4,
    title: 'Admission',
  },
  score: 23453,
  min60: 20845,
  mean60: 21345,
  max60: 23415,
  interestedCount: 10,
};

export default dummyInfo;
